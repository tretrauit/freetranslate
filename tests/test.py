import unittest
import sys
import asyncio
sys.path.append('.')
from freetranslate import GoogleWebTranslate, GoogleTranslate, LibreTranslate, BingTranslator, TranslateResult

class TestCase(unittest.TestCase):
    def test_google_web_translate(self):
        self.assertTrue(isinstance(asyncio.run(GoogleWebTranslate().translate('Xin chào', 'en')), TranslateResult))
        #self.assertTrue(isinstance(asyncio.run(GoogleWebTranslate().translate('Hello everyone!', 'ja')), TranslateResult))

    def test_google_translate(self):
        self.assertTrue(isinstance(asyncio.run(GoogleTranslate().translate('Xin chào', 'en')), TranslateResult))
        #self.assertTrue(isinstance(asyncio.run(GoogleTranslate().translate('Hello everyone!', 'ja')), TranslateResult))

    def test_libretranslate(self):
        self.assertTrue(isinstance(asyncio.run(LibreTranslate().translate('Xin chào', 'en')), TranslateResult))
        #self.assertTrue(isinstance(asyncio.run(LibreTranslate().translate('Hello everyone!', 'ja')), TranslateResult))

    def test_bingtranslator(self):
        bing_chilling = BingTranslator()
        self.assertTrue(isinstance(asyncio.run(bing_chilling.translate('Xin chào', 'en')), TranslateResult))
        #self.assertTrue(isinstance(asyncio.run(BingTranslator().translate('Hello everyone!', 'ja')), TranslateResult))

if __name__ == '__main__':
    unittest.main()